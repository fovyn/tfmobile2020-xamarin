﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TodoList.DAL.Models
{
    /// <summary>
    /// Modèle représentant le User en base de données locale
    /// </summary>
    [Table("users")]
    public class User
    {
        [PrimaryKey, AutoIncrement]
        public long Id { get; set; }
        [Unique, NotNull]
        public string Email { get; set; }
        [NotNull]
        public string Password { get; set; }

    }
}
