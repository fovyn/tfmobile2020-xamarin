﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoList.DAL.Repositories
{
    public interface ICrudRepository<TValue, TKey>
    {
        TValue Create(TValue obj);
        List<TValue> FindAll();
        TValue FindOneById(TKey id);
        TValue Update(TKey id, TValue obj);
        bool Delete(TKey id);
    }
}
