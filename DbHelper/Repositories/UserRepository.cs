﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoList.DAL.Models;

namespace TodoList.DAL.Repositories
{
    public interface UserRepository: ICrudRepository<User, long>
    {
        User FindOneByEmail(string email);
        bool HasData();
    }
}
