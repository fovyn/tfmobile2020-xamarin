﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoList.DAL.Models;
using System.Linq;
using SQLite;

namespace TodoList.DAL.Repositories
{
    public class UserRepositoryImpl : BaseRepositoryImpl, UserRepository
    {
        public TableMapping TableMapping { get; private set; }
        public UserRepositoryImpl()
        {
            TableMapping = new TableMapping(typeof(User));
        }
        public User Create(User obj)
        {
            Connection.Insert(obj);

            return this.FindOneByEmail(obj.Email);
        }

        public bool Delete(long id)
        {
            return Connection.Delete(id, TableMapping) != 0;
        }

        public List<User> FindAll()
        {
            return Connection.Table<User>().ToList();
        }

        public User FindOneByEmail(string email)
        {
            return Connection.Table<User>().Where(u => u.Email == email).FirstOrDefault();
        }

        public User FindOneById(long id)
        {
            return Connection.Table<User>().Where(u => u.Id == id).FirstOrDefault();
        }

        public User Update(long id, User obj)
        {
            obj.Id = id;
            Connection.Update(obj);

            return this.FindOneById(id);
        }

        public bool HasData()
        {
            return this.FindAll().Count > 0;
        }
    }
}
