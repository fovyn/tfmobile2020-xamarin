﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TodoList.DAL.Repositories
{
    public abstract class BaseRepositoryImpl
    {
        protected SQLiteConnection Connection = DbHelper.Instance.Connection;
    }
}
