﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using TodoList.DAL.Models;
using TodoList.DAL.Repositories;

namespace TodoList.DAL
{
    public class DbHelper
    {
        public const string DB_NAME = "user.db3";

        #region Singleton
        private static DbHelper _Instance = null;
        public static DbHelper Instance => _Instance = _Instance ?? new DbHelper();
        #endregion

        public SQLiteConnection Connection { get; set; }
        private Dictionary<Type, object> Repositories = new Dictionary<Type, object>();

        private DbHelper()
        {
            Connection = new SQLiteConnection(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), DB_NAME));
            Connection.CreateTable(typeof(User));
            Repositories.Add(typeof(UserRepository), new UserRepositoryImpl());
        }

        public T GetRepository<T>(Type type) where T: class
        {
            return Repositories[type] as T;
        }
    }
}
