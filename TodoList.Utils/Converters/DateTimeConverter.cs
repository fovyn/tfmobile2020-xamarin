﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace TodoList.Utils.Converters
{
    public class DateTimeConverter: IsoDateTimeConverter
    {
        public DateTimeConverter(): this("yyyy-MM-dd")
        {

        }
        public DateTimeConverter(string format)
        {
            this.DateTimeFormat = format;
        }
    }
}
