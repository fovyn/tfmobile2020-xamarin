﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace TodoList.Utils.Forms
{
    public static class ValidationHelper
    {
        public static bool IsFormValid(object model, Action<ValidationResult, string> errorsHandler)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(model);
            bool isValid = Validator.TryValidateObject(model, context, errors, true);

            if (!isValid)
            {
                foreach(ValidationResult validationResult in errors)
                {
                    foreach(string propertyName in validationResult.MemberNames)
                    {
                        errorsHandler?.Invoke(validationResult, propertyName);
                    }
                }
            }

            return isValid;
        }
    }
}
