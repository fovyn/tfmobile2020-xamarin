﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TodoList.Utils.Converters;

namespace TodoList.Models.Entities
{
    /// <summary>
    /// Modèle représentant l'entité User de l'API 
    /// </summary>
    public class User
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [Required]
        [EmailAddress]
        [JsonProperty("email")]
        public string Email { get; set; }
        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("createAt")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime CreateAt { get; set; }
        [JsonProperty("updateAt")]
        [JsonConverter(typeof(DateTimeConverter), "yyyy-MM-dd")]
        public DateTime UpdateAt { get; set; }
        [Required]
        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }
}
