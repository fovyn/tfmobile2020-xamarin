﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.DAL;
using TodoList.DAL.Models;
using TodoList.DAL.Repositories;
using TodoList.Pages;
using Xamarin.Forms;

namespace TodoList
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            if (!DbHelper.Instance.GetRepository<UserRepository>(typeof(UserRepository)).HasData())
            {
                Navigation.PushAsync(new LoginPage()); // Intent
            }
        }
    }
}
