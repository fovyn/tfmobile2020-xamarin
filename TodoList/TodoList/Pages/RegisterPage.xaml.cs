﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TodoList.Models.Entities;
using TodoList.Utils.Forms;
using Xamarin.Android.Net;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TodoList.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();

            eEmail.Text = "Flavian";
            ePassword.Text = "Blop";
        }

        /// <summary>
        /// Methode permettant la gestion du click sur le bouton S'enregistrer
        /// </summary>
        /// <param name="sender">Bouton sur lequel j'ai cliqué</param>
        /// <param name="args">Evenement du click</param>
        public void OnRegisterAction(object sender, EventArgs args)
        {
            #region Récupération des valeurs de mes edit
            string email = eEmail.Text;
            string password = ePassword.Text;
            string confirmPassword = eConfirmPassword.Text;
            #endregion

            User user = new User() { Email = email, Password = password };

            #region Validation Input
            Email_Error.IsVisible = false;
            Password_Error.IsVisible = false;
            ConfirmPassword_Error.IsVisible = false;
            PasswordAreNotSame_Error.IsVisible = false;
            if (ValidationHelper.IsFormValid(user, ErrorHandler) && password == confirmPassword)
            {
                //Digest = Resultat d'un algorithme de Hashage
                //Nuget used => BCrypt.Net-PCL
                var digest = BCrypt.Net.BCrypt.HashPassword(password, BCrypt.Net.BCrypt.GenerateSalt());
                user.Password = digest;
                user.CreateAt = DateTime.Now;
                user.UpdateAt = DateTime.Now;
                user.IsActive = true;

                HttpClient httpClient = new HttpClient(new AndroidClientHandler());

                //Nuget used => NewtonSoft.Json
                string json = JsonConvert.SerializeObject(user);

                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                //10.0.2.2 => Alias pour localhost ==> Merci Pol
                httpClient.PostAsync("http://10.0.2.2:3000/api/users", content);
                Navigation.PopModalAsync();             
            } 
            else if (password != confirmPassword)
            {
                PasswordAreNotSame_Error.Text = "Password and confirmPassword are not the same";
                PasswordAreNotSame_Error.IsVisible = true;
            }
            #endregion
        }

        private void ErrorHandler(ValidationResult validationResult, string propertyName)
        {
            Label label = null;

            switch (propertyName)
            {
                case "Email": label = Email_Error; break;
                case "Password": label = Password_Error; break;
                case "ConfirmPassword": label = ConfirmPassword_Error; break;
                default: label = null; break;
            }

            if (label != null)
            {
                label.Text = validationResult.ErrorMessage;
                label.IsVisible = true;
            }
        }

        private void ToggleErrorLabel(params Tuple<Label, string>[] labels)
        {
            foreach((Label label, string message) in labels)
            {
                label.Text = message;
                label.IsVisible = true;
            }
        }
    }
}