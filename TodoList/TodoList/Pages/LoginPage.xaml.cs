﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.DAL;
using dal = TodoList.DAL.Models;
using api = TodoList.Models.Entities;
using TodoList.DAL.Repositories;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Xamarin.Android.Net;
using Newtonsoft.Json;

namespace TodoList.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private UserRepository Repository;
        public LoginPage()
        {
            InitializeComponent();
            Repository = DbHelper.Instance.GetRepository<UserRepository>(typeof(UserRepository));
        }

        public async Task OnSignInAction(object sender, EventArgs args)
        {
            string email = eEmail.Text;
            string password = ePassword.Text;

            HttpClient client = new HttpClient(new AndroidClientHandler());

            HttpResponseMessage response = await client.GetAsync($"http://10.0.2.2:3000/api/users?email={email}");
            List<api.User> users = JsonConvert.DeserializeObject<List<api.User>>(await response.Content.ReadAsStringAsync());

            if (users.Count == 1)
            {
                api.User toConnect = users[0];
                if (BCrypt.Net.BCrypt.CheckPassword(password, toConnect.Password))
                {
                    Repository.Create(new dal.User() { Email = toConnect.Email, Password = toConnect.Password });
                    await DisplayAlert("Connection", "Connection success", "OK");
                }
            }

        }

        public void OnRegisterAction(object sender, EventArgs args)
        {
            Navigation.PushModalAsync(new RegisterPage());
        }
    }
}